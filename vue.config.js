module.exports = {
    lintOnSave: true,
    publicPath: process.env.NODE_ENV === 'production'? './' : './',
    devServer: {
        port: 1996,
        open: true,
        proxy: {
            "/Home": {
                target: " https://dyh.muyu007.cn",
                changeOrigin: true
            }
        }
    }

}