
/***
 *
 * 实例
 * export const getOrderList = (data) => POST('/Home/Cashier/getCartCount',data);
 * export const getOrderTmp = (data) => GET('/Home/Cashier/getOrderTmp',data);
 * 
 * 页面中使用
 * import { getOrderList, getOrderTmp } from "../../api/api"; // 导入我们的api接口
 * getOrderList({uid: -1,cart: [],coupon_id: 0,wipe_zore: 0,discount: {},}).then((res) => {
 *    console.log(res);
 * });
 * getOrderTmp({storeid: 0,shopid: 3,}).then((res) => {
 *    console.log(res)
 * })
 * */

/**
* api接口统一管理,无任何业务逻辑上的需求请勿修改任何代码！！
*/
 import { GET, POST } from './http'
